
<!DOCTYPE HTML>
<html>
<head>
    @yield('title-head')
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!--//Metis Menu -->
    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
    <!-- Custom CSS -->
    <link href="css/style.css" rel='stylesheet' type='text/css' />
    <!-- font CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
    <!-- font-awesome icons -->
    <link href="css/font-awesome.css" rel="stylesheet">
    <!-- //font-awesome icons -->
    <!-- js-->
    <script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/scripts.js"></script>
    <!--//scrolling js-->
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.js"> </script>
    <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <!--//webfonts-->

    <!-- Metis Menu -->
    <script src="js/metisMenu.min.js"></script>
    <script src="js/custom.js"></script>
    <link href="css/custom.css" rel="stylesheet">
    <style>
        hr {
            border: 0;
            max-width: 400px;
            height: 1px;
            background-image:linear-gradient(to right, rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.75), rgba(0, 0, 0, 0));
        }
        p {
            font-size: 12pt;
            line-height: 1.6;
        }
        input {
            font-family: FontAwesome;}
        #page-wrapper{background:url('violate.jpg');  }
        .panel-heading{padding:0px !important;}
        .panel{background-color: rgba(255, 255, 255, 0.69);
        border-radius:10px;}
    </style>
</head>
<body class="cbp-spmenu-push cbp-spmenu-push-toright">
<div class="main-content">
    <!-- header-starts -->
    <div class="sticky-header header-section ">
        <div class="header-left">
            <!--logo -->
            <div class="logo">
                <a href="">
                    <h1>BOX</h1>
                    <span>Invoices Control</span>
                </a>
            </div>
            <!--//logo-->
            <div class="page-title">
                <h1>@yield('titre')</h1>
            </div>
        </div>
    </div>
    <!-- //header-ends -->
    <!-- main content start-->
    <div id="page-wrapper">
        <div class="main-page">
            <!---------------------------------------------- main-------------------------------------------------->
            @yield('content')

        </div>
    </div>
    <!--footer-->
    <div class="footer">
        <p class="text-center">&copy; 2016 <a href="#">BOX </a>Admin Panel.</p>
    </div>
    <!--//footer-->
</div>
</body>
</html>