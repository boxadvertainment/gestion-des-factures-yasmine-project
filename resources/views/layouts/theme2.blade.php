<!DOCTYPE HTML>
<html>
<head>
    @yield('title-head')
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!--//scrolling js-->
   {{-- <link href="{{ url ('css/bootstrap.css') }}"  rel='stylesheet' type='text/css' />
    <link href="{{ url ('css/font-awesome.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{ url ('css/clndr.css')}}" type="text/css" />
    <link href="{{ url ('css/style.css')}}" rel='stylesheet' type='text/css' />
    <link href="{{ url ('css/custom.css')}}" rel="stylesheet">
    <!-- font CSS -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

--}}
    <!--webfonts-->
    <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <!--//webfonts-->

    <!--//Metis Menu -->
    <style>
        input {
            font-family: FontAwesome;}
    </style>
</head>
<body class="cbp-spmenu-push">
<div class="main-content">
    <!--left-fixed -navigation-->
    <div class=" sidebar" role="navigation">
        <div class="navbar-collapse">
            <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
                <ul class="nav" id="side-menu">
                        <li>
                            <a href="{{URL('admin/first')}}"><i class="fa fa-home nav_icon"></i>Dashboard</a>
                        </li>
                    @if(Auth::user()->role==1)
                    <li>
                        <a href="{{URL('admin/listclient')}}"><i class="fa fa fa-users nav_icon"></i>Clients list</a>
                    </li>
                    @endif
                    <li>
                        <a href="#"><i class="fa fa-book nav_icon"></i>Invoices information<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level collapse">
                            <li>
                                <a href="{{URL('admin/factures')}}">General</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <div class="clearfix"> </div>
                <!-- //sidebar-collapse -->
            </nav>
        </div>
    </div>
    <!--left-fixed -navigation-->
    <!-- header-starts -->
    <div class="sticky-header header-section ">
        <div class="header-left">
            <!--toggle button start-->
            <button id="showLeftPush"><i class="fa fa-bars"></i></button>
            <!--toggle button end-->
            <!--logo -->
            <div class="logo">
                <a href="">
                    <h1>BOX</h1>
                    @if(Auth::user()->role==1)
                        <span>AdminPanel</span>
                    @else
                        <span>ClientPanel</span>
                    @endif
                </a>
            </div>
            <!--//logo-->
            <div class="page-title">
                <h1>@yield('titre')</h1>
            </div>
        </div>
        <div class="header-right">
            <div class="profile_details_left"><!--notifications of menu start -->
                <ul class="nofitications-dropdown">
                    <li class="dropdown head-dpdn">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-bell"></i><span class="badge blue">3</span></a>
                        <ul class="dropdown-menu">
                            <li>
                                <div class="notification_header">
                                    <h3>You have 3 new notification</h3>
                                </div>
                            </li>
                            <li class="odd"><a href="#">
                                    <div class="notification_desc">
                                        <p>Lorem ipsum dolor amet </p>
                                        <p><span>1 hour ago</span></p>
                                    </div>
                                    <div class="clearfix"></div>
                                </a></li>
                            <li>
                                <div class="notification_bottom">
                                    <a href="#">See all notifications</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>

            </div>
            <!--notification menu end -->
            <div class="profile_details">
                <ul>
                    <li class="dropdown profile_details_drop">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <div class="profile_img">
                                <div class="user-name">
                                    <p>{{ Auth::user()->name }}</p>
                                    @if(Auth::user()->role==1)
                                        <span>Administrator</span>
                                    @else
                                        <span>Client</span>
                                    @endif
                                </div>
                                <i class="fa fa-angle-down lnr"></i>
                                <i class="fa fa-angle-up lnr"></i>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                        <ul class="dropdown-menu drp-mnu">

                            <li> <a href="{{ url('/logout') }}"><i class="fa fa-sign-out"></i> Logout</a> </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="clearfix"> </div>
        </div>
        <div class="clearfix"> </div>
    </div>
    <!-- //header-ends -->
    <!-- main content start-->
    <!---------------------------------------------- main-------------------------------------------------->
            @yield('content')
    <!--footer-->
    <div class="footer">
        <p class="text-center">&copy; 2016 <a href="#">BOX </a>Admin Panel.</p>
    </div>
    <!--//footer-->
</div>
{{--<script src="{{ url ('js/jquery-1.11.1.min.js')}}"></script>
<script src="{{ url ('js/classie.js')}}"></script>
<script src="{{ url ('js/jquery.nicescroll.js')}}"></script>
<script src="{{ url ('js/scripts.js')}}"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{ url ('js/bootstrap.js')}}"> </script>
<script src="{{ url ('js/modernizr.custom.js')}}"></script>
<!-- Metis Menu -->
<script src="{{ url ('js/metisMenu.min.js')}}"></script>
<script src="{{ url ('js/custom.js')}}"></script>

<script src="{{ url ('js/Chart.js')}}"></script>
<script src="{{ url ('js/underscore-min.js')}}" type="text/javascript"></script>
<script src= "{{ url ('js/moment-2.2.1.js')}}" type="text/javascript"></script>
<script src="{{ url ('js/clndr.js')}}" type="text/javascript"></script>
<script src="{{ url ('js/site.js')}}" type="text/javascript"></script>--}}
<!-- Classie -->
<script>
    var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
            showLeftPush = document.getElementById( 'showLeftPush' ),
            body = document.body;

    showLeftPush.onclick = function() {
        classie.toggle( this, 'active' );
        classie.toggle( body, 'cbp-spmenu-push-toright' );
        classie.toggle( menuLeft, 'cbp-spmenu-open' );
        disableOther( 'showLeftPush' );
    };

    function disableOther( button ) {
        if( button !== 'showLeftPush' ) {
            classie.toggle( showLeftPush, 'disabled' );
        }
    }
</script>
</body>
</html>