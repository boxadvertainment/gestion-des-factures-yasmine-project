@extends('layouts.theme')
<!----------le titre de a page ------------->
@section('title-head')
    <title>Inbox</title>
    <!-- Bootstrap Core CSS -->
    <link href="../../css/bootstrap.css" rel='stylesheet' type='text/css' />
    <!-- Custom CSS -->
    <link href="../../css/style.css" rel='stylesheet' type='text/css' />
    <!-- font CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
    <!-- font-awesome icons -->
    <link href="../../css/font-awesome.css" rel="stylesheet">
    <!-- //font-awesome icons -->
    <!-- js-->
    <script src="../../js/jquery-1.11.1.min.js"></script>

    <script src="../../js/jquery.nicescroll.js"></script>
    <script src="../../js/classie.js"></script>
    <script src="../../js/scripts.js"></script>
    <!--//scrolling js-->
    <!-- Bootstrap Core JavaScript -->
    <script src="../../js/bootstrap.js"> </script>
    <script src="../../js/modernizr.custom.js"></script>
    <!--webfonts-->
    <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <!--//webfonts-->

    <!-- Metis Menu -->
    <script src="../../js/metisMenu.min.js"></script>
    <script src="../../js/custom.js"></script>
    <link href="../../css/custom.css" rel="stylesheet">
@endsection
<!----------titre de la section ------------->
@section('titre')
    INBOX
@endsection
<!----------le main de la page------------->
@section('content')
    <div class="main-page compose">
        <div class="col-md-4 compose-left">
            <div class="folder widget-shadow">
                <ul>
                    <li class="head">Last Comments</li>
                    @if($comments->isEmpty())
                        <li><a href="#">
                                <div class="chat-right">
                                    <p style="color:#C16172;">You Have No Comment</p>
                                </div>
                                <div class="clearfix"> </div>
                            </a>
                        </li>
                    @else
                        @foreach($comments as $comment)
                        <li><a href="#">
                                <div class="chat-right">
                                    <p>{{$clients->name_client}}</p>
                                    <h5>{{$comment->msg}}</h5>
                                    <h6>{{$comment->created_at->format('d M Y')}}</h6>
                                </div>
                                <div class="clearfix"> </div>
                            </a>
                        </li>
                        @endforeach
                    @endif
                </ul>
            </div>
        </div>
        <div class="col-md-8 compose-right widget-shadow">
            <div class="panel-default">
                <div class="panel-heading">
                    Compose New Comment
                </div>
                <div class="panel-body">
                    <div class="alert alert-info">
                        Please fill details to send a new Comment
                    </div>
                    <form role="form" class="com-mail"  method="post" href="{{url('admin/savecomment',array($clients->id_facture))}}">
                        {{ csrf_field('delete')}}
                        <textarea  id="msg" name="msg" rows="6" class="form-control1 control2" placeholder="comment :" required></textarea>
                        <input type="hidden" id="id_facture" name="id_facture" value="{{$clients->id_facture}}">
                        <input type="hidden" id="id_client" name="id_client" value="{{$clients->id_client}}">
                        <input type="submit" value="Send">
                    </form>
                    <br>
                    <h4><a class="col-xs-1" href="{{url('admin/factures')}}">back</a></h4>
                </div>
            </div>
        </div>
        <div class="clearfix"> </div>
    </div>
@endsection