<!----------le theme ------------->
@extends('layouts.theme')
<!----------Edit form------------->
@section('Form2')
    <form  id="modifier" role="form" method="POST">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-6 form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name" class="control-label">Name :</label>
                <div>
                    <input id="nameC" type="text" class="form-control" name="name" value="" placeholder="&#xf040;">

                    @if ($errors->has('name'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="col-md-6 form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class=" control-label">E-Mail Address :</label>

                <div >
                    <input id="emailC" type="email" class="form-control" name="email" value="" placeholder="&#xf0e0;">

                    @if ($errors->has('email'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="col-md-6 form-group{{ $errors->has('MF_client') ? ' has-error' : '' }}">
                <label for="MF_client" class=" control-label">MF :</label>

                <div >
                    <input id="MFC" type="text" class="form-control" name="MF_client" value="" placeholder="&#xf040;">

                    @if ($errors->has('MF_client'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('MF_client') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="col-md-6 form-group{{ $errors->has('phone-client') ? ' has-error' : '' }}">
                <label for="phone_client" class=" control-label">Phone Number :</label>
                <div >
                    <input id="phoneC" type="text" class="form-control" name="phone_client" value="" placeholder="&#xf095;">

                    @if ($errors->has('phone_client'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('phone_client') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <div class=" col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-user"></i>Update
                    </button>
                </div>
            </div>
        </div>
    </form>
@endsection
<!----------Add client Form------------->
{{--@section('BodyForm')
    <form  role="form" method="POST" action="{{ url('admin/register') }}">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-6 form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="name" class="control-label">Name :</label>

                <div>
                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="&#xf040;">

                    @if ($errors->has('name'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="col-md-6 form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                <label for="email" class=" control-label">E-Mail Address :</label>

                <div >
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="&#xf0e0;">

                    @if ($errors->has('email'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="col-md-6 form-group{{ $errors->has('MF_client') ? ' has-error' : '' }}">
                <label for="MF_client" class=" control-label">MF :</label>

                <div >
                    <input id="MF_client" type="text" class="form-control" name="MF_client" value="{{ old('MF_client') }}" placeholder="&#xf040;">

                    @if ($errors->has('MF_client'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('MF_client') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="col-md-6 form-group{{ $errors->has('phone_client') ? ' has-error' : '' }}">
                <label for="phone_client" class=" control-label">Phone Number :</label>
                <div >
                    <input type="text" class="form-control" name="phone_client" value="{{ old('phone_client') }}" placeholder="&#xf095;">

                    @if ($errors->has('phone_client'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('phone_client') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>


            <div class="col-md-6 form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class=" control-label">Password :</label>

                <div>
                    <input id="password" type="password" class="form-control" name="password" placeholder="&#xf023;">

                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="col-md-6 form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <label for="password-confirm" class=" control-label">Confirm Password :</label>

                <div >
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="&#xf023;">

                    @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group">
                <div class=" col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-user"></i> Register
                    </button>
                </div>
            </div>
        </div>
    </form>
@endsection
@section('form')
    <div class="row ">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-heading">
                <hr>
                <h2 class="intro-text text-center">Register New Client</h2>
                <hr>
                <div class="panel-body">
                @yield('BodyForm')
                </div>
            </div>
        </div>
    </div>
@endsection--}}
<!----------le titre de a page ------------->
@section('title-head')
 <title>Clients</title>
 <link href="../css/bootstrap.css" rel='stylesheet' type='text/css' />
 <!-- Custom CSS -->
 <link href="../css/style.css" rel='stylesheet' type='text/css' />
 <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" type='text/css' >
 <!-- font CSS -->
 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
 <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
 <!-- font-awesome icons -->
 <link href="../css/font-awesome.css" rel="stylesheet">
 <!-- //font-awesome icons -->
 <!-- js-->
 <script src="../js/jquery-1.11.1.min.js"></script>
 <script src="../js/classie.js"></script>
 <script src="../js/jquery.nicescroll.js"></script>
 <script src="../js/scripts.js"></script>
 <!--//scrolling js-->
 <!-- Bootstrap Core JavaScript -->
 <script src="../js/bootstrap.js"> </script>
 <script src="../js/modernizr.custom.js"></script>
 <!--webfonts-->
 <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
 <!--//webfonts-->

 <!-- Metis Menu -->
 <script src="../js/metisMenu.min.js"></script>
 <script src="../js/custom.js"></script>
 <link href="../css/custom.css" rel="stylesheet">
@endsection
<!----------titre de la section ------------->
@section('titre')
    Clients List
@endsection
<!----------le main de la page------------->
@section('content')
    @if($clients->where('role',0)->count()==0)
     <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">No client available</h3>
            </div>
            <div class="panel-body">
             <div >
                if you want to add Client just here <i class="fa fa-level-down"></i> </div>
                <a href="{{url('/register')}}" {{--data-toggle="modal" data-target="#formModal"--}} ><i class=" fa fa-user-plus"> </i></a>
            </div>
            @yield('form')
        </div>
    @else
    <div class="tables">
        <div class="bs-example widget-shadow" data-example-id="hoverable-table">
            <h4>New : <a href="{{url('/register')}}" {{--data-toggle="modal" data-target="#formModal"--}} ><i class=" fa fa-user-plus"> </i></a></h4>

            <table class="table table-hover " id="myTable">
            <thead>
              <tr>
                <th>ID </th>
                <th>Name </th>
                  <th>Email</th>
                      <th>Phone number</th>
                    <th>Action</th>
                    </tr>
            </thead>
            <tbody>
              @foreach($clients as $key=> $client)
                  @if($client->role==0)
              <tr>
                <td id="clientid{{$client->id}}">{{$client->id}}</td>
                <td>{{$client->name}}</td>
                <td>{{$client->email}}</td>
                <td>{{$client->phone_client}}</td>
                <td>
                  <a href="" class="btn-modal" data-toggle="modal" data-target="#gridSystemModal"
                             data-nbfact="{{ $client->getNumberFacture() }}"
                             data-paid="{{ $client->getNumberFacturePaid() }}"
                             data-unpaid="{{ $client->getNumberFactureUnpaid() }}"
                             data-name="{{ $client->name }}"
                             data-email="{{ $client->email }}"
                             data-matricule="{{$client->MF_client}}"
                             data-date="{{ $client->created_at->format('d M Y') }}"
                             data-phone="{{ $client->phone_client}}"><i class="fa fa-info"  style="font-size:30px"></i></a>
                  <a href="" class="btn-modal2" data-toggle="modal" data-target="#form2Modal"
                     data-idc = "{{ $client->id }}"
                     data-namec="{{ $client->name }}"
                     data-emailc="{{ $client->email}}"
                     data-mfc="{{ $client->MF_client }}"
                     data-phonec="{{ $client->phone_client}}"><i class="fa fa-pencil-square-o" style="font-size:30px"></i></a>
                  <a href="{{URL('admin/factureC',array($client->id))}}"><i class="fa fa-eye" style="font-size:30px"></i></a>
                  <a data-toggle="tooltip" data-placement="right" data-original-title="This will delete also all the invoices of this Client" href="{{URL('admin/deleteClient',array($client->id))}}"><i class="fa fa-trash-o" style="font-size:30px"></i></a>
                    <script>$(function () {
                            $('[data-toggle="tooltip"]').tooltip()
                        })</script>
                </td>
              </tr>
                  @endif
              @endforeach
            </tbody> </table>
            <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
            <script>
                $(document).ready(function(){
                    $('#myTable').DataTable({
                        "aLengthMenu": [[5, 10,-1], [5,10, "All"]],
                        "iDisplayLength":5
                    });
                });
            </script>
        </div>
    </div>
    <!----------Form Add client Modal------------->
   {{-- <div class=" modal-grids">
        <div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">Add Client</h4>
                    </div>
                    <div class="modal-body">
                       @yield('BodyForm')
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>--}}
    <!----------Update Modal------------->
    <div class=" modal-grids">
        <div class="modal fade" id="form2Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="exampleModalLabel">Update</h4>
                    </div>
                    <div class="modal-body">
                        @yield('Form2')
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!----------more information Modal------------->
    <div class="modal-grids">
        <div class="modal fade" id="gridSystemModal" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
            <div class="modal-dialog" role="document">
                    <div class="col-xs-offset-3 modal-body">
                        <div class=" row">
                            <div class="col-md-12 profile widget-shadow">
                                <div class="profile-top">
                                    <a class=" close" data-dismiss="modal">&times;</a>
                                    <h4 id="name-client"></h4>
                                    <h5>MF : <h5 id="matricule-client"></h5> </h5>
                                </div>
                                <div class="profile-text">
                                    <div class="profile-row">
                                        <div class="profile-left">
                                            <i class="fa fa-envelope profile-icon"></i>
                                        </div>
                                        <div class="profile-right">
                                            <h4 id="email-client"></h4>
                                            <p>Email</p>
                                        </div>
                                        <div class="clearfix"> </div>
                                    </div>
                                    <div class="profile-row row-middle">
                                        <div class="profile-left">
                                            <i class="fa fa-mobile profile-icon"></i>
                                        </div>
                                        <div class="profile-right">
                                            <h4 id="phone-client"></h4>
                                            <p>Contact Number</p>
                                        </div>
                                        <div class="clearfix"> </div>
                                    </div>
                                    <div class="profile-row">
                                        <div class="profile-left">
                                            <i class="fa fa-calendar profile-icon"></i>
                                        </div>
                                        <div class="profile-right">
                                            <h4 id="date-at"></h4>
                                            <p>Date of inscription</p>
                                        </div>
                                        <div class="clearfix"> </div>
                                    </div>
                                </div>
                                <div class="profile-btm">
                                    <ul>
                                        <li>
                                            <h4 id="nb-fact"></h4>
                                            <h5>All invoices</h5>
                                        </li>
                                        <li>
                                            <h4 id="paid-fact"></h4>
                                            <h5>Paid invoices</h5>
                                        </li>
                                        <li>
                                            <h4 id="unpaid-fact"></h4>
                                            <h5>Unpaid invoices</h5>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                    </div>
                        </div>

                <!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>
    @endif

    <script>
        $(document).ready(function(){
            //view Client Modal
            $('.btn-modal').click(function(){
                $('#name-client').empty().append($(this).data('name'));
                $('#phone-client').empty().append($(this).data('phone'));
                $('#email-client').empty().append($(this).data('email'));
                $('#date-at').empty().append($(this).data('date'));
                $('#nb-fact').empty().append($(this).data('nbfact'));
                $('#paid-fact').empty().append($(this).data('paid'));
                $('#matricule-client').empty().append($(this).data('matricule'));
                $('#unpaid-fact').empty().append($(this).data('unpaid'));
            });
            //Edit Client Modal
            $('.btn-modal2').click(function(){
                $('#modifier').attr({
                    action: "{{ URL::to('admin/updateClient/') }}/"+$(this).data('idc')
                });
                $('#nameC').attr({
                    value: $(this).data('namec')

                });
                $('#emailC').attr({
                    value: $(this).data('emailc')
                });
                $('#MFC').attr({
                    value: $(this).data('mfc')
                });
                $('#phoneC').attr({
                   value: $(this).data('phonec')
                });
            });
        });
    </script>
@endsection
