@extends('layouts.theme')
<!----------le titre de a page ------------->
@section('title-head')
    <title>Invoices</title>
    <!-- Bootstrap Core CSS -->
    <link href="../css/bootstrap.css" rel='stylesheet' type='text/css' />
    <!-- Custom CSS -->
    <link href="../css/style.css" rel='stylesheet' type='text/css' />
    <!-- font CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
    <!-- font-awesome icons -->
    <link href="../css/font-awesome.css" rel="stylesheet">
    <!-- //font-awesome icons -->
    <!-- js-->

    <script src="../js/jquery-1.11.1.min.js"></script>
    <script src="../js/classie.js"></script>
    <script src="../js/jquery.nicescroll.js"></script>
    <script src="../js/scripts.js"></script>
    <!--//scrolling js-->
    <!-- Bootstrap Core JavaScript -->
    <script src="../js/bootstrap.js"> </script>
    <script src="../js/modernizr.custom.js"></script>
    <!--webfonts-->
    <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <!--//webfonts-->

    <!-- Metis Menu -->
    <script src="../js/metisMenu.min.js"></script>
    <script src="../js/custom.js"></script>
    <link href="../css/custom.css" rel="stylesheet">
@endsection
<!----------titre de la section ------------->
@section('titre')
    Invoices
@endsection
<!----------Add invoices Form------------->
@section('BodyForm')
        <form  role="form" method="POST" action="{{ url('admin/save') }}">
            {{ csrf_field() }}
            <div class="row">

                <div class="col-md-6">
                    <label for="mySelect" class="control-label">Client name:</label>
                        <select id="mySelect" name="mySelect" class="form-control1" onchange="changeValue();">
                            <option value="">Clients Name</option>
                            @foreach($users as $user)
                                @if($user->role==0)
                                <option value="{{$user->id}}">{{$user->name}}</option>
                                @endif
                            @endforeach
                        </select>
                    <input type="hidden" name="name_client" id="name-client">
                    <input type="hidden" name="id_client" id="id-client">
                </div>

                <div class="col-md-6 ">
                    <label for="date_facture" class="control-label">date facture:</label>
                    <input  id="date_facture" type="date" class="form-control1" name="date_facture" required>
                </div>

                <div class="col-md-6">
                    <label for="statut-form" class=" control-label">statut facture :</label>
                    <select class="form-control1" name="statut-form" id="statut-form" onchange="changeStatut();">
                        <option value='null'>- choose -</option>
                        <option value="1">Paid</option>
                        <option value="0">Unpaid</option>
                    </select>
                    <input id="statut_facture" type="hidden" class="form-control" name="statut_facture">
                    <script type="text/javascript">
                        function changeValue(){
                            var y=document.getElementById("mySelect").selectedIndex;
                            var x=document.getElementById("mySelect").options;
                            document.getElementById("name-client").value=x[y].text;
                            document.getElementById("id-client").value=x[y].value;}
                        function changeStatut(){
                            var a=document.getElementById("statut-form").selectedIndex;
                            var b=document.getElementById("statut-form").options;
                            if(b[a].value!='null')
                            {document.getElementById("statut_facture").value=b[a].value;}}
                    </script>
                </div>

                <div class="col-md-6">
                    <label for="projet_facture" class=" control-label">projet :</label>
                    <input id="projet_facture" type="text" class="form-control1" name="projet_facture" required>

                </div>

                <div class="col-md-6 ">
                    <label for="reference_facture" class=" control-label">reference facture:</label>
                    <input id="reference_facture" type="text" class="form-control1" name="reference_facture" required>

                </div>

                <div class="col-md-6 ">
                    <label for="montant_facture" class=" control-label">montant facture :</label>

                    <input id="montant_facture" type="text" class="form-control1" name="montant_facture" required >

                </div>

                <div class="form-group">
                    <div class=" col-md-12 text-center">
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
            </div>
        </form>
@endsection
@section('form')
    <div class="row ">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-heading">
                <hr>
                <h2 class="intro-text text-center">Add New Invoice</h2>
                <hr>
                <div class="panel-body">
                    @yield('BodyForm')
                </div>
            </div>
        </div>
    </div>
@endsection
<!----------Edit form------------->
@section('Form2')
    <form  id="edit" role="form" method="POST">
        {{ csrf_field() }}
        <div class="row">

            <div class="col-md-6">
                <label for="name_c" class="control-label">Client name:</label>
                    <input id="name_c" type="text" class="form-control1" name="name_c" disabled>
                    <input id="idC" type="hidden" class="form-control" name="id_client">
                    <input id="nameF" type="hidden" class="form-control" name="name_client">
            </div>

            <div class="col-md-6 ">
                <label for="dateF" class="control-label">date facture:</label>
                <input  id="dateF" type="date" class="form-control1" name="date_facture" required>
            </div>

            <div class="col-md-6">
                <label for="statutfac" class=" control-label">statut facture :</label>
                <select class="form-control1" name="statut" id="statutfac" onchange="getval(this);">
                    <option value="1">Paid</option>
                    <option value="0">Unpaid</option>
                </select>
                <input id="statutF" type="hidden" class="form-control" name="statut_facture">
                <script type="text/javascript">
                    function getval(sel) {
                        document.getElementById('statutF').value = sel.value;
                    }
                </script>
            </div>

            <div class="col-md-6">
                <label for="projetF" class=" control-label">projet :</label>
                <input id="projetF" type="text" class="form-control1" name="projet_facture" required>

            </div>

            <div class="col-md-6 ">
                <label for="ref" class=" control-label">reference facture:</label>
                <input type="text" id="ref" class="form-control1" name="reference_facture" required>

            </div>

            <div class="col-md-6 ">
                <label for="montantF" class=" control-label">montant facture :</label>

                <input id="montantF" type="text" class="form-control1" name="montant_facture" required >

            </div>

            <div class="form-group">
                <div class=" col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
            </div>
        </div>
    </form>
@endsection
<!----------le main de la page------------->
@section('content')
    @if($factures->isEmpty())
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">no invoices available</h3>
            </div>
            <div class="panel-body">
                <div >
                    If you want to add an invoice just add information here <i class="fa fa-level-down"></i> </div>
            </div>
            @yield('form')
        </div>
    @else
        <div class="tables bs-example widget-shadow" >
            @section('all')
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Invoice's ID</th>
                        <th>Name </th>
                        <th>Project</th>
                        <th>Reference</th>
                        <th>Date</th>
                        <th>amount</th>
                        <th>status</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($factures as $key=> $fact)
                        <tr>
                            <td id="factid{{$fact->id_facture}}">{{$fact->id_facture}}</td>
                            <td>{{$fact->name_client}}</td>
                            <td>{{$fact->projet_facture}}</td>
                            <td>{{$fact->reference_facture}}</td>
                            <td>{{$fact->date_facture}}</td>
                            <td>{{$fact->montant_facture}}</td>
                            <td>@if($fact->statut_facture== 1)
                                    Paid
                                @else
                                    Unpaid
                                @endif
                            </td>
                            <td>
                                <a href="" class="btn-modal2" data-toggle="modal" data-target="#form2Modal"
                                   data-idf = "{{ $fact->id_facture}}"
                                   data-namec = "{{ $fact->name_client}}"
                                   data-idc = "{{ $fact->id_client}}"
                                   data-projetf="{{ $fact->projet_facture }}"
                                   data-ref="{{$fact->reference_facture}}"
                                   data-datef="{{$fact->date_facture}}"
                                   data-montantf="{{ $fact->montant_facture}}"
                                   @if($fact->statut_facture==1)
                                   data-status="1"
                                   @else
                                   data-status="0"
                                   @endif
                                   data-namef="{{$fact->name_client}}"><i class="fa fa-pencil-square-o" style="font-size:30px"></i></a>
                                <a href="{{URL('admin/delete',array($fact->id_facture))}}"><i class="fa fa-trash-o" style="font-size:30px"></i></a>
                              {{-- <a href="{{URL('admin/email',array($fact->id_facture))}}"><i class="fa fa-comment-o" style="font-size:30px"></i></a>--}}
                                @if(Auth::user()->role==0)
                                <a href="{{URL('admin/email',array($fact->id_facture))}}"><i class="fa fa-comment-o" style="font-size:30px"></i></a>
                                @else
                                <a href="{{URL('admin/inbox',array($fact->id_facture))}}"><i class="fa fa-comment-o" style="font-size:30px"></i></a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody> </table>
            @endsection
                @section('unpaid')
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Invoice's ID</th>
                            <th>Name </th>
                            <th>Project</th>
                            <th>Reference</th>
                            <th>Date</th>
                            <th>amount</th>
                            <th>status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($factures as $key=> $fact)
                            @if($fact->statut_facture== 0)
                            <tr>
                                <td id="factid{{$fact->id_facture}}">{{$fact->id_facture}}</td>
                                <td>{{$fact->name_client}}</td>
                                <td>{{$fact->projet_facture}}</td>
                                <td>{{$fact->reference_facture}}</td>
                                <td>{{$fact->date_facture}}</td>
                                <td>{{$fact->montant_facture}}</td>
                                <td>Unpaid</td>
                                <td>
                                    <a href="" class="btn-modal2" data-toggle="modal" data-target="#form2Modal"
                                       data-idf = "{{ $fact->id_facture}}"
                                       data-namec = "{{ $fact->name_client}}"
                                       data-idc = "{{ $fact->id_client}}"
                                       data-projetf="{{ $fact->projet_facture }}"
                                       data-ref="{{$fact->reference_facture}}"
                                       data-datef="{{$fact->date_facture}}"
                                       data-montantf="{{ $fact->montant_facture}}"
                                       data-status="0"
                                       data-namef="{{$fact->name_client}}"><i class="fa fa-pencil-square-o" style="font-size:30px"></i></a>
                                    <a href="{{URL('admin/delete',array($fact->id_facture))}}"><i class="fa fa-trash-o" style="font-size:30px"></i></a>
                                    @if(Auth::user()->role==0)
                                        <a href="{{URL('admin/email',array($fact->id_facture))}}"><i class="fa fa-comment-o" style="font-size:30px"></i></a>
                                    @else
                                        <a href="{{URL('admin/inbox',array($fact->id_facture))}}"><i class="fa fa-comment-o" style="font-size:30px"></i></a>
                                    @endif
                                </td>
                            </tr>
                            @endif
                        @endforeach
                        </tbody> </table>
                @endsection
                @section('paid')
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Invoice's ID</th>
                            <th>Name </th>
                            <th>Project</th>
                            <th>Reference</th>
                            <th>Date</th>
                            <th>amount</th>
                            <th>status</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($factures as $key=> $fact)
                             @if($fact->statut_facture== 1)
                            <tr>
                                <td id="factid{{$fact->id_facture}}">{{$fact->id_facture}}</td>
                                <td>{{$fact->name_client}}</td>
                                <td>{{$fact->projet_facture}}</td>
                                <td>{{$fact->reference_facture}}</td>
                                <td>{{$fact->date_facture}}</td>
                                <td>{{$fact->montant_facture}}</td>
                                <td>Paid</td>
                                <td>
                                    <a href="" class="btn-modal2" data-toggle="modal" data-target="#form2Modal"
                                       data-idf = "{{ $fact->id_facture}}"
                                       data-namec = "{{ $fact->name_client}}"
                                       data-idc = "{{ $fact->id_client}}"
                                       data-projetf="{{ $fact->projet_facture }}"
                                       data-ref="{{$fact->reference_facture}}"
                                       data-datef="{{$fact->date_facture}}"
                                       data-montantf="{{ $fact->montant_facture}}"
                                       data-status="1"
                                       data-namef="{{$fact->name_client}}"><i class="fa fa-pencil-square-o" style="font-size:30px"></i></a>
                                    <a href="{{URL('admin/delete',array($fact->id_facture))}}"><i class="fa fa-trash-o" style="font-size:30px"></i></a>
                                    @if(Auth::user()->role==0)
                                        <a href="{{URL('admin/email',array($fact->id_facture))}}"><i class="fa fa-comment-o" style="font-size:30px"></i></a>
                                    @else
                                        <a href="{{URL('admin/inbox',array($fact->id_facture))}}"><i class="fa fa-comment-o" style="font-size:30px"></i></a>
                                    @endif
                                </td>
                            </tr>
                            @endif
                        @endforeach
                        </tbody> </table>
                @endsection
        </div>
        <!----------Form Add bill Modal------------->
        <div class=" modal-grids">
            <div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="exampleModalLabel">Add Invoice</h4>
                        </div>
                        <div class="modal-body">
                            @yield('BodyForm')
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!----------Update bill Modal------------->
        <div class=" modal-grids">
            <div class="modal fade" id="form2Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="exampleModalLabel">Edit </h4>
                        </div>
                        <div class="modal-body">
                            @yield('Form2')
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <script>
        $(document).ready(function(){
            //Edit Client Modal
            $('.btn-modal2').click(function(){
                $('#edit').attr({
                    action: "{{ URL::to('admin/update/') }}/"+$(this).data('idf')
                });
                $('#nameF').attr({
                    value: $(this).data('namec')
                });
                $('#name_c').attr({
                    value: $(this).data('namec')
                });
                $('#idC').attr({
                    value: $(this).data('idc')
                });
                $('#statutF').attr({
                    value: $(this).data('status')
                });
                $('#ref').attr({
                    value: $(this).data('ref')
                });
                if($(this).data('status') ==0){ $("#statutfac").val('0');}
                if($(this).data('status')  ==1){$("#statutfac").val('1');}
                $('#montantF').attr({
                    value: $(this).data('montantf')
                });
                $('#projetF').attr({
                    value: $(this).data('projetf')
                });
                $('#dateF').attr({
                    value: $(this).data('datef')
                });
            });
        });
    </script>

    <div class=" general-grids grids-right widget-shadow">
        <h4>New : <a href="" data-toggle="modal" data-target="#formModal" ><i class="fa fa-plus"> </i></a></h4>
        <ul id="myTabs" class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a href="#all" id="all-tab" role="tab" data-toggle="tab" aria-controls="all" aria-expanded="true">All invoices <span class="badge badge-primary">{{$fact->count()}}</span></a>
            </li>
            <li role="presentation" >
                <a href="#paid-in" role="tab" id="paid-in-tab" data-toggle="tab" aria-controls="paid-in" aria-expanded="false">Paid invoices <span class="badge badge-primary">{{$fact->where('statut_facture','1')->count()}}</span></a>
            </li>
            <li role="presentation" >
                <a href="#unpaid-in" role="tab" id="unpaid-in-tab" data-toggle="tab" aria-controls="unpaid-in" aria-expanded="false">Unpaid invoices <span class="badge badge-primary">{{$fact->where('statut_facture','0')->count()}}</span></a>
            </li>
            </ul>
        <div tabindex="5001" id="myTabContent" class="tab-content ">
            <div role="tabpanel" class="tab-pane fade active in" id="all" aria-labelledby="all-tab">
                @yield('all')
            </div>
            <div role="tabpanel" class="tab-pane fade " id="paid-in" aria-labelledby="paid-in-tab">
               @yield('paid')
            </div>
            <div role="tabpanel" class="tab-pane fade" id="unpaid-in" aria-labelledby="unpaid-in-tab">
              @yield('unpaid')
            </div>
             </div>
    </div>
@endsection