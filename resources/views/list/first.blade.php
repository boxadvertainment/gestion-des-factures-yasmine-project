@extends('layouts.theme2')
<!----------le titre de a page ------------->
@section('title-head')
    <title>Home</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{ url ('css/bootstrap.css') }}"  rel='stylesheet' type='text/css' />
    <script src="{{ url ('js/Chart.js')}}"></script>
    <!-- //chart -->
    <!--Calender-->
    <link rel="stylesheet" href="{{ url ('css/clndr.css')}}" type="text/css" />
    <script src="{{ url ('js/underscore-min.js')}}" type="text/javascript"></script>
    <script src= "{{ url ('js/moment-2.2.1.js')}}" type="text/javascript"></script>
    <script src="{{ url ('js/clndr.js')}}" type="text/javascript"></script>
    <script src="{{ url ('js/site.js')}}" type="text/javascript"></script>
    <!--End Calender-->
    <!-- Custom CSS -->
    <link href="{{ url ('css/style.css')}}" rel='stylesheet' type='text/css' />
    <!-- font CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
    <!-- font-awesome icons -->
    <link href="{{ url ('css/font-awesome.css')}}" rel="stylesheet">
    <!-- //font-awesome icons -->
    <!-- js-->
    <script src="{{ url ('js/jquery-1.11.1.min.js')}}"></script>
    <script src="{{ url ('js/classie.js')}}"></script>
    <script src="{{ url ('js/jquery.nicescroll.js')}}"></script>
    <script src="{{ url ('js/scripts.js')}}"></script>
    <!--//scrolling js-->
    <!-- Bootstrap Core JavaScript -->
    <script src="{{ url ('js/bootstrap.js')}}"> </script>
    <script src="{{ url ('js/modernizr.custom.js')}}"></script>
    <!--webfonts-->
    <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <!--//webfonts-->

    <!-- Metis Menu -->
    <script src="{{ url ('js/metisMenu.min.js')}}"></script>
    <script src="{{ url ('js/custom.js')}}"></script>
    <link href="{{ url ('css/custom.css')}}" rel="stylesheet">
@endsection
<!----------titre de la section ------------->
@section('titre')
Home
@endsection
<!----------le main de la page------------->
@section('content')
    <!-- chart -->
        <!-- main content start-->
    <div id="page-wrapper">
        <div class="main-page">
            @if(Auth::user()->role==0)
            <div class="widget_1 row elements">
                <div class="col-xs-offset-4 col-xs-4 widget_1_box widget-shadow">
                    <div class="tile-progress bg-info">
                        <div class="content">
                            <h4><i class="fa fa-dashboard icon-sm"></i>invoices</h4>
                            <div class="progress progress-striped active">
                                <div class="bar  blue" style="width: {{($factures->where('name_client',Auth::user()->name)->where('statut_facture','0')->count())*100/($factures->where('name_client',Auth::user()->name)->count())}}%;"></div>
                            </div>
                            <span>{{($factures->where('name_client',Auth::user()->name)->where('statut_facture','1')->count())*100/$factures->where('name_client',Auth::user()->name)->count()}}% Paid</span>
                        </div>
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>
            @else
            <div class="row-one">
                <div class="col-md-4 widget">
                    <div class="stats-left ">
                        <h5>Total</h5>
                        <h4>Invoices</h4>
                    </div>
                    <div class="stats-right">
                        <label>{{$factures->count()}}</label>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="col-md-4 widget states-mdl">
                    <div class="stats-left">
                        <h5>Total</h5>
                        <h4>Amount</h4>
                    </div>
                    <div class="stats-right">
                        <label>{{$factures->sum('montant_facture')}}</label>
                    </div>
                    <div class="clearfix"> </div>
                </div>
                <div class="col-md-4 widget states-last">
                    <div class="stats-left">
                        <h5>Total</h5>
                        <h4>Clients</h4>
                    </div>
                    <div class="stats-right">
                        <label>{{$clients->where('role',0)->count()}} </label>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"> </div>
            </div>
            <br><br>
            <div class="charts">
                <div class="col-md-offset-4 charts-grids widget">
                    <h4 class="title">Invoices Paid percentage </h4>
                    <canvas id="pie" height="300" width="400"> </canvas>
                </div>
                <div class="clearfix"> </div>

                <script>

                    var pieData = [
                        {
                            value:{{$factures->where('statut_facture','1')->count()}},
                            color:"rgba(79, 82, 186, 1)",
                            label: "Paid Invoices"
                        },
                        {
                            value :{{$factures->where('statut_facture','0')->count()}},
                            color : "rgba(242, 179, 63, 1)",
                            label: "Unpaid Invoices"
                        }

                    ];
                    new Chart(document.getElementById("pie").getContext("2d")).Pie(pieData);

                </script>

            </div>
            <div class="clearfix"> </div>
             @endif
        </div>
    </div>
@endsection
