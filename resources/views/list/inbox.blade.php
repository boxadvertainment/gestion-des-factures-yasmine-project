@extends('layouts.theme')
<!----------le titre de a page ------------->
@section('title-head')
    <title>Inbox</title>
    <!-- Bootstrap Core CSS -->
    <link href="../../css/bootstrap.css" rel='stylesheet' type='text/css' />
    <!-- Custom CSS -->
    <link href="../../css/style.css" rel='stylesheet' type='text/css' />
    <!-- font CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
    <!-- font-awesome icons -->
    <link href="../../css/font-awesome.css" rel="stylesheet">
    <!-- //font-awesome icons -->
    <!-- js-->
    <script src="../../js/jquery-1.11.1.min.js"></script>

    <script src="../../js/jquery.nicescroll.js"></script>
    <script src="../../js/classie.js"></script>
    <script src="../../js/scripts.js"></script>
    <!--//scrolling js-->
    <!-- Bootstrap Core JavaScript -->
    <script src="../../js/bootstrap.js"> </script>
    <script src="../../js/modernizr.custom.js"></script>
    <!--webfonts-->
    <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <!--//webfonts-->

    <!-- Metis Menu -->
    <script src="../../js/metisMenu.min.js"></script>
    <script src="../../js/custom.js"></script>
    <link href="../../css/custom.css" rel="stylesheet">
@endsection
<!----------titre de la section ------------->
@section('titre')
    INBOX
@endsection
<!----------le main de la page------------->
@section('content')
    <div class="main-page">
        <h3 class="title1">Comments</h3>
        <div class="inbox-page row">
            @if($comments->isEmpty())
                <div class="panel panel-danger">
                    <div class="panel-heading">
                        <h3 class="panel-title">No client available</h3>
                    </div>
                    <div class="panel-body">
                        <div >You have no Comment yet </div>
                    </div>
                </div>
            @else
            @foreach($clients as $client)
                @foreach($comments as $comment)
            <div class="inbox-row widget-shadow">
                <div class="mail mail-name"><h6>{{$client->name_client}}</h6></div>
                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
                    <div class="mail"><p>Click To see Comment</p></div>
                </a>
                <div class="mail-right">
                    <div class="dropdown">
                        <a href="#"  data-toggle="dropdown" aria-expanded="false">
                            <p><i class="fa fa-ellipsis-v mail-icon"></i></p>
                        </a>
                        <ul class="dropdown-menu float-right">
                            <li>
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="true" aria-controls="collapsefour">
                                    <i class="fa fa-reply mail-icon"></i>
                                    Reply
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="mail-right"><p>{{$comment->created_at->format('Y M D')}}</p></div>
                <div class="clearfix"> </div>
                <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                    <div class="mail-body">
                            <p>{{$comment->msg}}</p>
                        <form method="post" href="#">
                            <input type="text" placeholder="Reply to sender" required="">
                            <input type="submit" value="Send">
                        </form>
                    </div>
                </div>
            </div>
                    @endforeach
                @endforeach
            @endif

        </div>
    </div>

@endsection