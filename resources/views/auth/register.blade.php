@extends('layouts.themeLOGIN')
@section('title-head')
    <title>Register</title>
@endsection
@section('titre')
   register
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-heading">
              <hr>
                <h2 class="intro-text text-center">Add Client</h2>
              <hr>
                <div class="panel-body">
                    <form  role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}
                          <div class="row">
                        <div class="col-md-6 form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="control-label">Name :</label>

                            <div>
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="&#xf040;">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-6 form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class=" control-label">E-Mail Address :</label>

                            <div >
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="&#xf0e0;">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-6 form-group{{ $errors->has('MF_client') ? ' has-error' : '' }}">
                            <label for="MF_client" class=" control-label">MF :</label>

                            <div >
                                <input id="MF_client" type="text" class="form-control" name="MF_client" value="{{ old('MF_client') }}" placeholder="&#xf040;">

                                @if ($errors->has('MF_client'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('MF_client') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-6 form-group{{ $errors->has('phone_client') ? ' has-error' : '' }}">
                            <label for="phone_client" class=" control-label">Phone Number :</label>
                          <div >
                                <input type="text" class="form-control" name="phone_client" value="{{ old('phone_client') }}" placeholder="&#xf095;">

                                @if ($errors->has('phone_client'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone_client') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="col-md-6 form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class=" control-label">Password :</label>

                            <div>
                                <input id="password" type="password" class="form-control" name="password" placeholder="&#xf023;">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-md-6 form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class=" control-label">Confirm Password :</label>

                            <div >
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="&#xf023;">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                              <div class="form-group col-md-6">
                                  <label for="role" class="control-label">Role of User :</label>
                                  <div class="checkbox" >
                                      <label> <input type="checkbox" name="role1" id="role1"> Admin </label>
                                      <input type="hidden"  name="role" id="role">
                                  </div>
                                  <script>
                                      if(document.getElementById('role1').checked== true){
                                          document.getElementById('role').value='1';
                                      }
                                      else {
                                          document.getElementById('role').value='0';
                                      }
                                  </script>
                              </div>
                        <div class="form-group">
                            <div class=" col-md-12 text-center">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Register
                                </button>
                            </div>
                        </div>
                      </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
