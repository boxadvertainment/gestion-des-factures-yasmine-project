<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facture extends Model
{
protected $primaryKey = 'id_facture';
protected $table='factures';

public function user()
{
  return $this->belongsTo('App\User');
}
  public function commentaire()
  {
    return $this->hasMany('App\Commentaire');
  }
}
