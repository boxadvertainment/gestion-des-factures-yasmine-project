<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Facture;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','MF_client','phone_client',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function facture()
    {
      return $this->hasMany('App\Facture');
    }
//all invoices of a client
    public function getNumberFacture(){
        return Facture::where('id_client', $this->id)->count();
    }
    //all paid invoices  of a client
    public function getNumberFacturePaid(){
        return      Facture::where('id_client', $this->id)
                            -> where('statut_facture',1)
                            ->count();
    }
    //all Unpaid invoices of a client
    public function getNumberFactureUnpaid(){
        return Facture::where('id_client', $this->id)
            -> where('statut_facture',0)
            ->count();
    }
}
