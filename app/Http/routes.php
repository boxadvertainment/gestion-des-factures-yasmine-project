<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*-------------------------------------------used -------------------------------------------------------------*/
Route::auth();
/*Route::get('/', function () {
    return view('welcome');
});*/
Route::get('admin/listclient','ListclientController@ClientInfo');
Route::get('admin/factureC/{id}','FactureController@factureclient');
Route::post('admin/register','ListclientController@save');
Route::post('admin/save','FactureController@save');
Route::get('admin/delete/{id}','FactureController@destroy');
Route::get('admin/deleteClient/{id}','ListclientController@destroy');
Route::post('admin/updateClient/{id}','ListclientController@update');
Route::post('admin/update/{id}','FactureController@update');
Route::get('admin/factures','FactureController@factureinfo');
Route::get('admin/first','ListclientController@firstView');
Route::get('admin/email/{id}','FactureController@commentclient');
Route::post('admin/savecomment/{id}','FactureController@comment');
Route::get('admin/inbox/{id}','FactureController@commentadmin');
/*--------------------------------------not used yet -----------------------------------------------------------*/


/*--------------------------------------  AUTH -----------------------------------------------------------*/

