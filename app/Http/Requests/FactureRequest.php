<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class FactureRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'id_facture'=>'',
          'date_facture'=>'required',
          'statut_facture'=>'required',
          'id_client'=>'',
          'name_client'=>'',
          'projet_facture'=>'required',
          'reference_facture'=>'required',
          'montant_facture'=>'required'

        ];
    }
}
