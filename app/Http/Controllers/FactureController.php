<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use View;
use App\Facture;
use App\Commentaire;
use App\Http\Requests;
use App\Http\Requests\FactureRequest;
use App\Http\Requests\CommentRequest;
class FactureController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

//----------------Facture par client ----------------------//
     public function factureclient($id)
     {
       //$admin=auth()->guard('admins');
       return  View::make('list.invoicePERclient',['factures'=>Facture::where('id_client',$id)->get(),'users'=>User::where('id',$id)->get()]);
     }
//----------------lien a la page de saisie du commentaire----------------------//
    public function commentadmin($id)
    {
        $comments=Commentaire::where('id_facture',$id)->get();
         $clients=Facture::where('id_facture',$id)->get();
        return view('list.inbox',compact('comments','clients'));
    }
    public function commentclient($id)
    {
        //$admin=auth()->guard('admins');
        $comments=Commentaire::where('id_facture',$id)->get();
        $clients=Facture::where('id_facture',$id)->first();
        return view('list.compose',compact('comments','clients'));
    }
    //----------------sauvgarder le commentaire ----------------------//
    public function comment($id,CommentRequest $request)
    {
        $up=new Commentaire;
        $up->id_client=$request->input('id_client');
        $up->id_facture=$id;
        $up->msg=$request->input('msg');
        $up->save();
        return back();
    }
//----------------page de tout les factures ----------------------//
     public function factureinfo()
     {
       //$admin=auth()->guard('admins');
       $factures=Facture::all();
       $users=User::all();
       return view('list.facturesList',compact('factures','users'));
     }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

  //---------------- Save factures ----------------------//

    public function save(FactureRequest $request)
    {
      $save=new Facture;
      $save->name_client=$request->input('name_client');
      $save->id_client=$request->input('id_client');
      $save->date_facture=$request->input('date_facture');
      $save->statut_facture=$request->input('statut_facture');
      $save->projet_facture=$request->input('projet_facture');
      $save->reference_facture=$request->input('reference_facture');
      $save->montant_facture=$request->input('montant_facture');
      $save->save();
      return back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


     //----------------update la  modification de la facture ----------------------//

    public function update(FactureRequest $request,$id)
    {
      $save= Facture::find($id);
      $save->statut_facture= $request->input('statut_facture');
      $save->date_facture=$request->input('date_facture');
      $save->projet_facture=$request->input('projet_facture');
      $save->reference_facture=$request->input('reference_facture');
      $save->montant_facture=$request->input('montant_facture');
      $save->save();
      return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    //----------------supprimer facture ----------------------//

    public function destroy($id)
    {
      Facture::where('id_facture',$id)->delete();
      return back();
    }


}
