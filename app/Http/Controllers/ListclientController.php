<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use View;
use Validator;
use App\Facture;
use App\Commentaire;
use App\Http\Requests;
use App\Http\Requests\FactureRequest;
use App\Http\Requests\UserRequest;
use App\Http\Requests\CommentRequest;

class ListclientController extends Controller
{
  public function __construct()
  {
    $this->middleware('auth');
  }
  
  public function destroy($id)
  {
    User::where('id',$id)->delete();
    Facture::where('id_client',$id)->delete();
    return back();
  }
  public function save(UserRequest $request)
  {
    $save=new User;
    $save->name=$request->input('name');
    $save->email=$request->input('email');
    $save->password=bcrypt($request->input('password'));
    $save->MF_client=$request->input('MF_client');
    $save->phone_client=$request->input('phone_client');
    $save->save();
    return redirect('admin/listclient');
  }
  public function update(UserRequest $request,$id)
  {
    $save= User::find($id);
    $save->name=$request->input('name');
    $save->email=$request->input('email');
    $save->password= bcrypt($request->input('password'));
    $save->MF_client=$request->input('MF_client');
    $save->phone_client=$request->input('phone_client');
    $save->save();
    return Redirect('admin/listclient');
  }
public function ClientInfo()
{
  //$admin=auth()->guard('admins');
  return View::make('list.listclient',['clients'=>User::all(),'factures'=>Facture::all()]);
}
  public function firstView ()
  {
    $clients=User::all();
    $factures=Facture::all();
    $comments=Commentaire::all();
    return view('list.first',compact('clients','factures','comments'));
  }

}
