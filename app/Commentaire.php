<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Commentaire extends Model
{
      protected $primaryKey = 'id_commentaire';
      protected $table='commentaires';

      public function facture()
      {
            return $this->belongsTo('App\Commentaire');
      }
}
