<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentairesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commentaires', function (Blueprint $table) {
        $table->increments('id_commentaire');
        $table->integer('id_client')->unsigned();
        $table->integer('id_facture')->unsigned();
        $table->string('msg');
        $table->timestamps(); });
        Schema::table('commentaires', function(Blueprint $table) {
            $table->foreign('id_client')->references('id')->on('users')
            ->onDelete('restrict')
            ->onUpdate('restrict');
            $table->foreign('id_facture')->references('id_facture')->on('factures')
                ->onDelete('restrict')
                ->onUpdate('restrict');
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('commentaires');
    }
}
