<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('factures', function (Blueprint $table) {
          $table->increments('id_facture');
           $table->string('fichier_facture');
           $table->date('date_facture');
           $table->string('statut_facture');
           $table->integer('id_client')->unsigned();
           $table->string('name_client');
           $table->string('projet_facture');
           $table->string('reference_facture');
           $table->string('montant_facture');
           $table->timestamps();
        });
        Schema::table('factures', function(Blueprint $table) {
           $table->foreign('id_client')->references('id')->on('users')
                   ->onDelete('restrict')
                   ->onUpdate('restrict');
          $table->foreign('name_client')->references('name')->on('users')
                   ->onDelete('restrict')
                   ->onUpdate('restrict');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('factures');
    }
}
