
<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $this->call(MainAdmin::class);
    }

}

class MainAdmin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
        'name'=>'adminstrator',
        'email'=>'admin@box.agency',
          'role'=>'1',
        'password'=>bcrypt('yassmine123'),
      ]);
    }
}
